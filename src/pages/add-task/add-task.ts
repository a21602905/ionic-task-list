import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TaskProvider } from '../../providers/task/task'

@IonicPage()
@Component({
  selector: 'page-add-task',
  templateUrl: 'add-task.html',
})
export class AddTaskPage {

  public task: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public provider: TaskProvider) {
  }

  addTask() {
    //add task to array
    this.provider.add(this.task);

    //go back to previous screen
    if ( this.navCtrl.canGoBack() ) {
      this.navCtrl.pop();
    }
  }

}
