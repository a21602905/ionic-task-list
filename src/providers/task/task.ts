
import { Injectable } from '@angular/core';

/*
  Generated class for the TaskProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TaskProvider {

  public tasks: string[];

  constructor() {
    console.log('Hello TaskProvider Provider');
    this.init();
  }

  init() {
    this.tasks = [];
    this.tasks.push("Tarefa 1");
    this.tasks.push("Tarefa 2");
    this.tasks.push("Tarefa 3");
    this.tasks.push("Tarefa 4");
    this.tasks.push("Tarefa 5");
    this.tasks.push("Tarefa 6");
    this.tasks.push("Tarefa 7");
  }

  delete(task: string) {
    let index = this.tasks.indexOf(task);

    if(index > -1){
      this.tasks.splice(index, 1);
    }
  }

  add(task: string){
    this.tasks.push(task);
  }

}
